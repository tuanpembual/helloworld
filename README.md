# Deploy Hello World to Docker Swarm

## Testing on local using Docker-Compose
* Install docker

## Build Image
```
docker-compose build
```

## Run db migrate
```
docker-compose run web rake db:create RAILS_ENV=production \
&& docker-compose run web rake db:migrate RAILS_ENV=production
```

## Up container
```
docker-compose up -d
```

## Down Container
```
docker-compose down
```

## Clean Unused Container
```
docker ps -a
docker rm $(docker ps -a -q)
```

## Clean Unudes Images
```
docker images
docker rmi $(docker images | grep "<none>" | awk "{print $3}")
```

## Add tag to Images and upload to docker hub
```
docker login
docker tag bb38976d03cf tuanpembual/helloworld
docker push tuanpembual/helloworld
```

# Swarm

* add docker-compose-prod.yml
* build compose

```
docker-compose -f docker-compose-prod.yml run web rake db:create RAILS_ENV=production \
&& docker-compose -f docker-compose-prod.yml run web rake db:migrate RAILS_ENV=production
docker-compose -f docker-compose-prod.yml down
docker stack deploy --compose-file docker-compose-prod.yml helloworld
```

# Know Issue
* Fail deploy from gitlab-ci to swarm | FIX
* Run db migrate from endpoint
