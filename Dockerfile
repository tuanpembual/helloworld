FROM ruby:2.5.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
WORKDIR /hello

ENV RAILS_ENV production

COPY Gemfile /hello/Gemfile
COPY Gemfile.lock /hello/Gemfile.lock
RUN bundle install --jobs 20 --retry 5 --without development test
COPY . /hello
